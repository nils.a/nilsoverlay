# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/x11-plugins/screenlets/screenlets-0.1.2.ebuild,v 1.2 2011/04/11 20:52:51 arfrever Exp $

EAPI=5
PYTHON_DEPEND="2"

inherit distutils eutils

DESCRIPTION="Individual screenlets"
HOMEPAGE="http://www.screenlets.org"
SRC_URI="http://code.launchpad.net/${PN}/trunk/${PV}/+download/${P}.tar.bz2"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=""

pkg_setup() {
	python_set_active_version 2
	python_pkg_setup
}

src_prepare() {
	distutils_src_prepare
	python_convert_shebangs -r 2 src
}

src_install() {
	distutils_src_install
}
