# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit versionator

MY_FOLDER_PV=$(get_version_component_range 1-2)-$(get_version_component_range 3)
MY_SRC_PN=${PN}src
DESCRIPTION="Informix-4GL compatible compiler"
HOMEPAGE="http://aubit4gl.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/Aubit%20Reasonably%20Stable%20Source/${MY_FOLDER_PV}/${MY_SRC_PN}.${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""
DEPEND=""
RDEPEND="${DEPEND}"
S=${WORKDIR}/${MY_SRC_PN}

#make does not run with -j3...
MAKEOPTS="${MAKEOPTS} -j1"

src_unpack() {
	unpack "${A}"
	epatch "${FILESDIR}"/Makefile-install.mki.patch
}

src_configure() {
	#overriding host is probably no good, but configure won't run with host set.
	econf --disable-prefix-check --host=
}

src_compile() {
	emake || die
	emake extra || die
}

src_install() {
	mkdir -p "${D}"/usr/etc/ld.so.conf.d/
	emake install \
		PREFIX="${D}"/usr \
		LIB_INSTALL_LINK="${D}"/usr/lib \
		BIN_INSTALL_LINK="${D}"/usr/bin \
		|| die
	dodoc BUGS.txt ChangeLog project.status README.txt subproject.status 
	dodoc todo todo.done
}
