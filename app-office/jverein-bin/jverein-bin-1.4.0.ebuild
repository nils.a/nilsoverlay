#TODO: Missing Header ? 

EAPI=2

MY_PN=${PN/-bin/}
MY_P=${MY_PN}.${PV}

DESCRIPTION="management-tool for a registered society"
HOMEPAGE="http://www.jverein.de/"
SRC_URI="http://www.jverein.de/download/download_log.php?dl=${MY_P}.zip -> ${P}.zip"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

# TODO: Depend on hibiscus.
RDEPEND=">=dev-java/jameica-1.10.0"

DEPEND=""

S=${WORKDIR}/${MY_PN}

src_install() {
	dodir /usr/lib/jameica/plugins/${MY_PN}
	insinto /usr/lib/jameica/plugins/${MY_PN}
	doins plugin.xml || die
	doins ${MY_PN}.jar || die

	dodir /usr/lib/jameica/plugins/${MY_PN}/lib
	insinto /usr/lib/jameica/plugins/${MY_PN}/lib
	doins lib/* || die
}
