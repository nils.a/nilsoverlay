EAPI="2"
JAVA_PKG_IUSE="doc source"
WANT_ANT_TASKS="ant-nodeps"

inherit eutils java-pkg-2 java-ant-2 versionator

MY_PV="$(get_version_component_range 1-2)"
JAMEICA_VERSION="1.10"

DESCRIPTION="A HBCI homebanking application based on hbci4java"
HOMEPAGE="http://www.willuhn.de/products/hibiscus/"
SRC_URI="http://www.willuhn.de/products/hibiscus/releases/${MY_PV}/hibiscus.src.zip"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

COMMON_DEP=">=dev-java/jameica-${JAMEICA_VERSION}.0
        ~net-libs/hbci4java-2.5.12
	dev-java/obantoo
	dev-java/supercsv"

RDEPEND=">=virtual/jre-1.5
        ${COMMON_DEP}"

DEPEND=">=virtual/jdk-1.5
        ${COMMON_DEP}"

S=${WORKDIR}/${PN}

java_prepare() {
	epatch "${FILESDIR}/${PV}-hbci4java-api.patch"
	epatch "${FILESDIR}/${PV}-hbci4java-so-filename.patch"
	
        rm -v ${S}/lib/*.{jar,so,dll,jnilib} || die

        java-pkg_jar-from --with-dependencies --into ${S}/lib/ hbci4java,obantoo,jameica,supercsv
}

src_compile() {
	eant -f build/build.xml jar $(use_doc javadoc)
}

src_install() {
	dodir /usr/lib/jameica/plugins/hibiscus
	insinto /usr/lib/jameica/plugins/hibiscus
	
	doins plugin.xml
        doins releases/${PV}-0/${PN}/${PN}.jar
	
	cp -R "${S}/updates" "${D}/usr/lib/jameica/plugins/hibiscus/" || die "Install failed!"
	cp -R "${S}/sql" "${D}/usr/lib/jameica/plugins/hibiscus" || die "Install failed!"

	dodir /usr/lib/jameica/plugins/hibiscus/lib

	cp -R lib/birt ${D}/usr/lib/jameica/plugins/hibiscus/lib

	dosym "$(java-pkg_getjars hbci4java)" /usr/lib/jameica/plugins/hibiscus/lib/
	dosym "$(java-pkg_getjars obantoo)" /usr/lib/jameica/plugins/hibiscus/lib/
	dosym "$(java-pkg_getjars supercsv)" /usr/lib/jameica/plugins/hibiscus/lib/

	newicon icons/${PN}-icon-16x16.png ${PN}-icon-16x16.png || die "newicon failed"
        newicon icons/${PN}-icon-32x32.png ${PN}-icon-32x32.png || die "newicon failed"
	newicon icons/${PN}-icon-64x64.png ${PN}-icon-64x64.png || die "newicon failed"
        make_desktop_entry /usr/bin/jameica "Hibiscus" ${PN}-icon-32x32 "Office;Finance"

        use doc && java-pkg_dojavadoc releases/${PV}-0/javadoc
        use source && java-pkg_dosrc src/de
}
