# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

MY_PN="neo.map"
DESCRIPTION="A consolemap for the neo keyboard layout"
HOMEPAGE="http://neo-layout.org/"
SRC_URI="http://wiki.neo-layout.org/browser/linux/console/${MY_PN}\
?rev=${PV}&format=raw -> ${MY_PN}"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="app-arch/gzip"
RDEPEND="sys-apps/kbd"

src_unpack() {
	gzip -c "${DISTDIR}"/${MY_PN} > neo.map.gz || die
}

src_install() {
	echo installing
	insinto /usr/share/keymaps/i386/neo/
	doins neo.map.gz
}

pkg_postinst() {
	elog "To enable the neo layout in console you have to set"
	elog "KEYMAP=\"neo\""
	elog "in /etc/conf.d/keymaps"
	elog
	elog "Also be sure to select a nice unicode consolefont"
	elog "to display all the new characters"
	elog "i.e. by setting"
	elog "CONSOLEFONT=\"lat9w-16\""
	elog "in /etc/conf.d/consolefont"
}
