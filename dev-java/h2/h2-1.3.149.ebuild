EAPI="2"
JAVA_PKG_IUSE="doc source test"

inherit eutils java-pkg-2

MY_PV="2011-01-07"

DESCRIPTION="Java SQL Database"
HOMEPAGE="http://www.h2database.com/"
SRC_URI="http://www.h2database.com/${PN}-${MY_PV}.zip"

LICENSE="|| ( EPL-1.0 H2-1.0 )"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND=">=virtual/jre-1.5"

DEPEND=">=virtual/jdk-1.5
	app-arch/unzip
	dev-java/tomcat-servlet-api:2.4
	dev-java/lucene:3.0
	dev-java/slf4j-api
	test? ( dev-java/slf4j-nop )
        ${COMMON_DEP}"

S=${WORKDIR}/${PN}

java_prepare() {
	epatch ${FILESDIR}/${PV}-offline.patch
	
	mkdir ${S}/ext
	java-pkg_jar-from --into ${S}/ext/ servlet-api-2.4,lucene-3.0,slf4j-api,slf4j-nop

	sh build.sh clean || die
}

src_test() {
	#sh build.sh test || die
	return 0 # Tests don't work on my machine. 
	#TODO: check whats is the problem and report upstream
}

src_compile() {

	sh build.sh jar $(use_doc javadoc) || die "Compilation failed"
}

src_install() {
	java-pkg_newjar bin/${P}.jar

	use doc && java-pkg_dojavadoc docs/javadoc
	use source && java-pkg_dosrc src
}
