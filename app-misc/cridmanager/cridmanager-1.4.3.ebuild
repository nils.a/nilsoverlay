# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils java-pkg-2 java-ant-2
DESCRIPTION="A Program to manage \"Siemens Gigaset M740 AV DVB-T Box\" Files"
HOMEPAGE="http://www.cridmanager.de/"
SRC_URI="mirror://sourceforge/${PN}/${P}-src.zip"
LICENSE="GPL-2"
MINOR_NUM=${PV%\.*}
SLOT="${MINOR_NUM}"
KEYWORDS="~x86 ~amd64"
IUSE="jikes source"
RESTRICT="nostrip"
COMMON_DEP="
	>=dev-java/log4j-1.2.9
	>=dev-java/commons-net-1.3.0"
DEPEND=">=virtual/jdk-1.5
	dev-java/ant-core
	app-arch/unzip
	source? ( app-arch/zip )
	jikes? ( >=dev-java/jikes-1.22 )"
RDEPEND=">=virtual/jre-1.5"

S=${WORKDIR}/${PN}-${MINOR_NUM/\./_}

src_unpack(){
 	mkdir -p ${S}/src
	mkdir -p ${S}/lib
	cd ${S}/src/
	unpack ${A}
	cd ../lib/
	java-pkg_jar-from log4j 
	java-pkg_jar-from commons-net
}

src_compile() {
	local antflags="jar"
	use jikes && antflags="${antflags} -Dbuild.compiler=jikes"
	cd ${S}
	cp ${FILESDIR}/build-${PV}.xml ./build.xml
	ant ${antflags} || die "compilation failed !"
}

src_install() {
	java-pkg_dojar ${S}/dist/${PN}.jar
	use source && java-pkg_dosrc ${S}/src
	java-pkg_dolauncher ${PN}-${MINOR_NUM} \
		--main net.sourceforge.cridmanager.Start
}
