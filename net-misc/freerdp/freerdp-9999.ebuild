# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit base cmake-utils git-2

EGIT_REPO_URI="git://github.com/FreeRDP/FreeRDP-1.0.git"

DESCRIPTION="A Remote Desktop Protocol Client, forked from rdesktop"
HOMEPAGE="http://www.freerdp.com/"
SRC_URI=""

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS=""
IUSE="alsa cups debug directfb pulseaudio test X xinerama"

RDEPEND="
	>=dev-libs/openssl-0.9.8a
	alsa? ( media-libs/alsa-lib )
	cups? ( net-print/cups )
	directfb? ( dev-libs/DirectFB )
	pulseaudio? ( media-sound/pulseaudio )
	X? ( x11-libs/libX11
		x11-libs/libXext
		x11-libs/libXcursor )
	xinerama? ( x11-libs/libXinerama )"

DEPEND="${RDEPEND}
	test? ( dev-util/cunit )"

DOCS=( README )

# `freerdp_use_with foo FOO`
# echoes -DWITH_FOO=ON -DWITHOUT_FOO=OFF if foo useflag is enabled
# and -DWITH_FOO=OFF -DWITHOUT_FOO=ON if foo useflag is disabled.
freerdp_use_with() {
    local opt=${2:-$(echo "${1}" | tr '[:lower:]' '[:upper:]')}

    if use "${1}"; then
        echo "-DWITH_${opt}=ON"
        echo "-DWITHOUT_${opt}=OFF"
    else
        echo "-DWITH_${opt}=OFF"
        echo "-DWITHOUT_${opt}=ON"
    fi
}

src_test() {
	"${CMAKE_BUILD_DIR}"/cunit/test_freerdp
}

src_configure() {
	local mycmakeargs="
		$(freerdp_use_with alsa)
		$(freerdp_use_with cups)
		$(freerdp_use_with directfb)
		$(freerdp_use_with pulseaudio)
		$(freerdp_use_with X X11)
		$(freerdp_use_with X XCURSOR)
		$(freerdp_use_with X XEXT)
		$(freerdp_use_with xinerama)
		$(cmake-utils_use_with debug DEBUG_CERTIFICATE)
		$(cmake-utils_use_with debug DEBUG_CHANMAN)
		$(cmake-utils_use_with debug DEBUG_DVC)
		$(cmake-utils_use_with debug DEBUG_GDI)
		$(cmake-utils_use_with debug DEBUG_KBD)
		$(cmake-utils_use_with debug DEBUG_LICENSE)
		$(cmake-utils_use_with debug DEBUG_NEGO)
		$(cmake-utils_use_with debug DEBUG_NLA)
		$(cmake-utils_use_with debug DEBUG_RFX)
		$(cmake-utils_use_with debug DEBUG_SVC)
		$(cmake-utils_use_with debug PROFILER)"

	cmake-utils_src_configure
}
