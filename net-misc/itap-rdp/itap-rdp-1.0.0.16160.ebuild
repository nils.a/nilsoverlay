# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4
inherit eutils

DESCRIPTION="A Remote Desktop Protocol Client. iTap RDP gives you complete control over and fast access to your Windows PC from anywhere."

HOMEPAGE="http://itap-mobile.com/desktop/rdp"
MY_PN_BASE="2011-07-27-qmote_Linux"
MY_SRC_URI_BASE="http://cf3.itap-mobile.com/downloads/qmote/${MY_PN_BASE}"
SRC_URI="x86? ( ${MY_SRC_URI_BASE}_i386-${PV}.tar.gz )
        amd64? ( ${MY_SRC_URI_BASE}_x86_64-${PV}.tar.gz )"
        
LICENSE=""
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

#DEPEND=""
RDEPEND="${DEPEND}"
if use x86 ; then
	S="${WORKDIR}/${MY_PN_BASE}_i386-${PV}"
else
	S="${WORKDIR}/${MY_PN_BASE}_x86_64-${PV}"
fi

src_unpack() {
	unpack "${A}"
}

src_install() {
	cd "${S}"
	insinto /opt/${PN}
	doins -r libs || die
	exeinto /opt/${PN}
	doexe qmote || die
	dodir /usr/bin || die
	dosym /opt/${PN}/qmote /usr/bin/ || die
	dosym /opt/${PN}/qmote /usr/bin/itap-rdp || die
}
