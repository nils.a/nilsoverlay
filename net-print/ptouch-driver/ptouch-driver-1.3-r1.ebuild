# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit autotools autotools-utils

DESCRIPTION="foomatic drivers for the Brother P-touch series of label printers"
HOMEPAGE="http://www.diku.dk/hjemmesider/ansatte/panic/P-touch/"
SRC_URI="http://www.diku.dk/hjemmesider/ansatte/panic/P-touch/${P}.tar.gz"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""
DEPEND="net-print/foomatic-db"
RDEPEND="${DEPEND}"

PATCHES=(
	"${FILESDIR}/ptouch-driver-1.3-add-Debug-option.patch"
	"${FILESDIR}/ptouch-driver-1.3-enable-DEBUG.patch"
	"${FILESDIR}/ptouch-driver-1.3-fix-dpi-for-QL5x0.patch"
)

src_prepare() {
	autotools-utils_src_prepare
	eautoreconf
}

src_install() {
	autotools-utils_src_install
	dosym /usr/lib/cups/filter/rastertoptch /usr/bin/rastertoptch || die
}
