# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/x11-plugins/pidgin-mbpurple/pidgin-mbpurple-0.3.0-r1.ebuild,v 1.1 2011/01/13 08:46:16 fauli Exp $

EAPI=2
EGIT_REPO_URI="git://github.com/nils-a/mbPurple.git"
EGIT_BRANCH="testing"
EGIT_PRUNE="1"
MY_P="mbPurple"

inherit base git-2 toolchain-funcs

DESCRIPTION="Libpurple (Pidgin) plug-in supporting microblog services like Twitter or identi.ca nils testing git branch"
HOMEPAGE="https://github.com/nils-a/mbPurple/tree/testing"

SRC_URI=""

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+twitgin"

RDEPEND="!x11-plugins/pidgin-mbpurple
	net-im/pidgin
	twitgin? ( net-im/pidgin[gtk] )"
DEPEND="${RDEPEND}
	dev-util/pkgconfig"
S=${WORKDIR}/${MY_P}

pkg_setup() {
	tc-export CC
}

