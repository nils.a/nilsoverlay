# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4
inherit eutils
DESCRIPTION="AeroFS is a p2p sync & storage client - currently only available for invited people."
HOMEPAGE="https://aerofs.com"
SRC_URI="aerofs-installer.deb"

pkg_nofetch() {
		einfo "Since AeroFS is only available for invited people"
		einfo "You'll have to get an invite which in turn gets You"
		einfo "a download-link. Go there and fetch ${SRC_URI}"
		einfo "and then place it in ${DISTDIR}."
}

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"
IUSE=""
RESTRICT="fetch"
DEPEND=""
RDEPEND="${DEPEND}"
S=${WORKDIR}

src_install() {
	tar xvzf ${S}/data.tar.gz -C ${ED}
}
